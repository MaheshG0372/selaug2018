package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortingList {

	public static void main(String[] args) 
	{
		String name ="Amazon India";
		List<Character> eachChar = new ArrayList<Character>();
		char[] allChar=name.toCharArray();
		
		for (char ch : allChar) 
		{
			eachChar.add(ch);
		}
		
		Collections.sort(eachChar);
		for (char sortedChar : eachChar) 
		{
			System.out.println(" After sorting : " +sortedChar);
		}
	}

}
