package week2.day1;

import java.util.*;

public class LearningList 
{

	public static void main(String[] args) 
	{
       List<String> myPhoneslist=new ArrayList<String>();
       myPhoneslist.add("nokia 7210");
       myPhoneslist.add("samsung Galaxy");
       myPhoneslist.add("Redmi Note 3");
       myPhoneslist.add("Huawei Nova 3i");
       for (String eachPhone : myPhoneslist) {
		System.out.println(eachPhone);
	}
       int size=myPhoneslist.size();
       System.out.println("Size of myPhones list is " + size);
       System.out.println("Last but not least mobile was " + myPhoneslist.get(size-2));
       Collections.sort(myPhoneslist);
       System.out.println("After sorting ");
       for (String sorted : myPhoneslist) 
       {
   		System.out.println(sorted);
   	   }
	}

}
