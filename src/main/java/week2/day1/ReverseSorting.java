package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReverseSorting {

	public static void main(String[] args)
	{
		String name ="Amazon India";
		List<Character> eachChar = new ArrayList<Character>();
		String lowerCase=name.toLowerCase();
        char[] upperChar=lowerCase.toCharArray();
		
		for (char ch : upperChar) 
		{
			eachChar.add(ch);
		}
		Collections.sort(eachChar);
		Collections.reverse(eachChar);
		for (char reverseChar : eachChar) 
		{
			System.out.println(" Reverse order : " +reverseChar);
		}
		
	}

}
