package week2.day1;

public class Redmi extends MobilePhone
{
	public void sendVoiceMessage()
	{
		System.out.println("Voice Message sent");
	}
	//Method overriding which present in Telecom class
	public void dialNumber()
	{
		System.out.println("Dail by voice from Redmi class");
	}
	//Method overloading
	public void dialNumber(int number)
	{
		System.out.println("dialNumber Method Overloading in Redmi class");
	}

}
