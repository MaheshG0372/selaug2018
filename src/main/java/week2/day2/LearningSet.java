package week2.day2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LearningSet {

	public static void main(String[] args) 
	{
		Set<String> mobilesSet=new HashSet<String>();
		mobilesSet.add("Nokia");
		mobilesSet.add("Moto");
		mobilesSet.add("Samsung");
		mobilesSet.add("Iphone");
		mobilesSet.add("Samsung");
		System.out.println(mobilesSet);
		List<String> list=new ArrayList<String>();
		list.addAll(mobilesSet);
		int size=list.size();
		System.out.println(list.get(size-1));
		
	}

}
