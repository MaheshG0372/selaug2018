package wdTestcasesOld;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import week4.day1.DragAndDrop;

public class SeMethods implements WdMethods{
	public RemoteWebDriver driver;
	public int i = 1;
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver  = new ChromeDriver();
			} 
			else if(browser.equalsIgnoreCase("firefox"))
			{
				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
				driver  = new FirefoxDriver();			
			}
			else if(browser.equalsIgnoreCase("opera")) 
			{
				System.setProperty("webdriver.opera.driver", "./drivers/operadriver.exe");
				driver  = new FirefoxDriver();			
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The Browser "+browser+" launched successfully");
		} catch (WebDriverException e) {
                  System.err.println("WebDriverException occured");		
                  }
		catch (NullPointerException e) {
            System.err.println("NullPointerException occured");		
            }
		catch(Exception e)
		{
			System.err.println("Exception occured");
		}
		finally
		{
			takeSnap();
		}

	}


	public WebElement locateElement(String locator, String locValue) {
		switch (locator) {
		case "id": return driver.findElementById(locValue);
		case "class": return driver.findElementByClassName(locValue);
		case "xpath": return driver.findElementByXPath(locValue);
		case "linktext": return driver.findElementByLinkText(locValue);	
		case "name" : return driver.findElementByName(locValue);
		case "tagname" : return driver.findElementByTagName(locValue);
		case "partiallinktext" : return driver.findElementByPartialLinkText(locValue);
		case "stylesheet" : return driver.findElementByCssSelector(locValue);
		}
		return null;
	}

	
	public WebElement locateElement(String locValue) {		
		return driver.findElementById(locValue);
	}

	public void type(WebElement ele, String data) {
		ele.sendKeys(data);
		System.out.println("The Data "+data+" is entered Successfully");
		takeSnap();
	}


	public void click(WebElement ele) {
		ele.click();
		System.out.println("The Element "+ele+" is clicked Successfully");
		takeSnap();
	}

	
	public String getText(WebElement ele) {
		
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
       Select dd=new Select(ele);
       dd.selectByVisibleText(value);
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		 Select dd=new Select(ele);
		 dd.selectByIndex(index);
	}

	@Override
	public boolean verifyTitle(String expectedTitle) 
	{
		boolean valid=false;
		String title=driver.getTitle();
		if(title.equals(expectedTitle))
		{
	      valid=true;
		}
		return valid;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) 
	{
      String txt=ele.getText();
      if(txt.equals(expectedText))
      {
    	  System.out.println("text matches exactly");
      }
      else
      {
    	  System.out.println("text not matches exactly");
      }
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) 
	{
		 String txt=ele.getText();
	      if(txt.contains(expectedText))
	      {
	    	  System.out.println("text matches partially");
	      }
	      else
	      {
	    	  System.out.println("text not matches partially");
	      }

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		String val= ele.getAttribute(attribute);
		if(val.equals(value))
		{
			System.out.println("Attribute is matching");
		}
		else
		{
			System.out.println("Attribute is not matching");

		}

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		String val= ele.getAttribute(attribute);
		if(val.contains(value))
		{
			System.out.println(" Partial Attribute is matching");
		}
		else
		{
			System.out.println(" Partial Attribute is not matching");

		}
	}

	@Override
	public void verifySelected(WebElement ele) 
	{
		boolean selected = ele.isSelected();
            if(selected)
            {
            	System.out.println("value is selected");
            }
            else
            {
            	System.out.println("value is not selected");
            }
	}

	@Override
	public void verifyDisplayed(WebElement ele) 
	{
		boolean displayed = ele.isDisplayed();
		if(displayed)
		{
			System.out.println("displayed");
		}
		else
		{
			System.out.println("not displayed");
		}

	}

	@Override
	public void switchToWindow(int index) 
	{
		Set<String> allwindows=driver.getWindowHandles();
		List<String> listofWindows=new ArrayList<String>();
		listofWindows.addAll(allwindows);
		driver.switchTo().window(listofWindows.get(index));
	}

	@Override
	public void switchToFrame(WebElement ele) {
              driver.switchTo().frame(ele);
              System.out.println("Frame switched");
	}

	@Override
	public void acceptAlert() {
            driver.switchTo().alert().accept();
	}

	@Override
	public void dismissAlert() {
        driver.switchTo().alert().dismiss();

	}

	@Override
	public String getAlertText() {
		String text = driver.switchTo().alert().getText();
		return text;
	}


	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File desc = new File("./snaps/img"+i+".png");		
		FileUtils.copyFile(src, desc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
       driver.close();   
	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub

	}

}
