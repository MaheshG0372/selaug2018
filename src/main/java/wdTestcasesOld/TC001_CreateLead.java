package wdTestcasesOld;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;


public class TC001_CreateLead extends SeMethods{
	
	@Test
	public void login() {
		startApp("chrome", "http://leaftaps.com/opentaps");
		WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("id","password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class", "decorativeSubmit");
		click(eleLogin);
		WebElement eleLink= locateElement("linktext", "CRM/SFA");
		click(eleLink);
		WebElement eleCreateLead= locateElement("linktext", "Create Lead");
		click(eleCreateLead);
		WebElement eleUsername = locateElement("id", "createLeadForm_firstName");
		type(eleUsername, "Mahesh");
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		type(eleLastName, "G");
		WebElement elefirstNamelocal = locateElement("id", "createLeadForm_firstNameLocal");
		type(elefirstNamelocal, "Mahesh");
		WebElement eleLastNamelocal = locateElement("id", "createLeadForm_lastNameLocal");
		type(eleLastNamelocal, "G");
		WebElement eleSalutation = locateElement("id", "createLeadForm_personalTitle");
		type(eleSalutation, "Mahesh");
		WebElement source= locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(source, "Employee");
		WebElement eleTitle = locateElement("id", "createLeadForm_generalProfTitle");
		type(eleTitle, "Mr");
		WebElement eleRevenue = locateElement("id", "createLeadForm_annualRevenue");
		type(eleRevenue, "4000");
		WebElement industry= locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingText(industry, "Telecommunications");
		WebElement ownerShip= locateElement("id", "createLeadForm_ownershipEnumId");
		selectDropDownUsingIndex(ownerShip, 1);
		
		
		
		//WebElement eleCompany = locateElement("id", "createLeadForm_companyName");
		//type(eleCompany, "Nmsworks");
		//WebElement submit = locateElement("class", "smallSubmit");
		//click(submit);
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		//click(eleLogout);
	}
	
}







