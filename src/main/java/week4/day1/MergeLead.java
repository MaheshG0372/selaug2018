package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead 
{

	public static void main(String[] args) 
	{
	System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	ChromeDriver driver=new ChromeDriver();
	driver.manage().window().maximize();
	driver.get("http://leaftaps.com/opentaps/");
	driver.findElementById("username").sendKeys("DemoSalesManager");
	driver.findElementById("password").sendKeys("crmsfa");
	driver.findElementByClassName("decorativeSubmit").click();
	driver.findElementByLinkText("CRM/SFA").click();
	driver.findElementByLinkText("Leads").click();
	driver.findElementByLinkText("Merge Leads").click();
	driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[1]").click();
	Set<String> allwindows=driver.getWindowHandles();
	List<String> listofWindows=new ArrayList<String>();
	listofWindows.addAll(allwindows);
	driver.switchTo().window(listofWindows.get(1));
	System.out.println(driver.getTitle());
	System.out.println(driver.getCurrentUrl());
	driver.findElementByXPath("(//input[@class=' x-form-text x-form-field '])[1]").sendKeys("10");
	//driver.findElementByClassName(" x-form-text x-form-field ").sendKeys("10");

	}

}
