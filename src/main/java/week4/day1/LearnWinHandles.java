package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWinHandles 
{
	public static void main(String[] args) throws IOException 
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/nget/");
		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.findElementByLinkText("Contact Us").click();
		Set<String> allwindows=driver.getWindowHandles();
		List<String> listofWindows=new ArrayList<String>();
		listofWindows.addAll(allwindows);
		driver.switchTo().window(listofWindows.get(1));
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		File src=driver.getScreenshotAs(OutputType.FILE);
		File img=new File("./snaps/contact.jpg");
		FileUtils.copyFile(src, img);
		driver.switchTo().window(listofWindows.get(0)).close();
		
	}

}
