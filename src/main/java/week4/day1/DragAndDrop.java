package week4.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class DragAndDrop {

	public static void main(String[] args) 
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://jqueryui.com/draggable/");
		Actions builder=new Actions(driver);
		WebElement frame=driver.findElementByClassName("demo-frame");
		driver.switchTo().frame(frame);
		WebElement drag=driver.findElementById("draggable");
		//drag.getLocation();
		builder.dragAndDropBy(drag, 100, 100).perform();
		

	}

}
