package wdMethods;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class FacebookLite 
{
	public static void main(String[] args) throws InterruptedException 
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.get("https://www.facebook.com/");
		driver.findElementById("email").sendKeys("mahesh.prince3@gmail.com");
		driver.findElementByXPath("(//input[@type='password'])[1]").sendKeys("09511a0511");
		driver.findElementByXPath("//input[@type='submit']").click();
		driver.findElementByXPath("//input[@type='text']").sendKeys("TestLeaf");
		Thread.sleep(5000);
		driver.findElementByXPath("//button[@data-testid='facebar_search_button']").click();
		WebElement clickbutton = driver.findElementByXPath("(//div[text()='TestLeaf']/following::button[1])");
		clickbutton.click();
		if(clickbutton.isSelected())
		{
			System.out.println("Clicked");
		}
		
		else
		{
		    System.out.println("Not clicked");	
		}
	}

}
