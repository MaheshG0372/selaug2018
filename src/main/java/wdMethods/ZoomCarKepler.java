package wdMethods;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ZoomCarKepler 
{	
	public static void main(String[] args) throws InterruptedException 
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.zoomcar.com/chennai/");
		driver.findElementByLinkText("Start your wonderful journey").click();
		driver.findElementByXPath("(//div[@class='items'])[2]").click();
		driver.findElementByClassName("proceed").click();
		driver.findElementByXPath("(//div[@class='day'])[1]").click();
		Thread.sleep(5000);
		driver.findElementByClassName("proceed").click();
		Thread.sleep(5000);
		driver.findElementByClassName("proceed").click();
		Thread.sleep(5000);
		List<WebElement> pp=driver.findElementsByClassName("price");
		List<Integer> lis=new ArrayList<Integer>();
		System.out.println("count of retrived items : " +pp.size());
		for(WebElement each : pp)
		{
			String pri= each.getText().replaceAll("\\D","");
			//System.out.println(pri);
			lis.add(Integer.parseInt(pri));		
		}
		Integer col=Collections.max(lis);
		System.out.println("Max price : "+col);
		String carname= driver.findElementByXPath("//div[contains(text(),"+col+")]//preceding::h3[1]").getText();
		System.out.println("Car Name:" + carname);
		driver.findElementByXPath("//div[contains(text(),"+col+")]//preceding::button").click();
		//System.out.println("Car Name:" + carname);
		
	
	}

}
