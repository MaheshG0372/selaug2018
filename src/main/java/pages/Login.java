package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import testNG.ProjectMethodsTestNG;

public class Login extends ProjectMethodsTestNG
{
  public Login()
  {
	  PageFactory.initElements(driver, this);
  }
  @FindBy(id="username")
  WebElement eleusername;
  public Login typeUsername(String data)
  {
	//  WebElement eleusername = locateElement("id", "username");
	  type(eleusername, data);
	  return this;
  }
  @FindBy(id="password")
  WebElement elePassword;
  public Login typePassword(String data)
  {
	 // WebElement elePassword = locateElement("id", "password");
	  type(elePassword, data);
	  return this;
  }
  @FindBy(how=How.CLASS_NAME,using ="decorativeSubmit")
  WebElement eleLogin;
  public HomePage clickLogin()
  {
	  //WebElement eleLogin = locateElement("class", "decorativeSubmit");
	  click(eleLogin);
	  return new HomePage();
  }
  
	
}
