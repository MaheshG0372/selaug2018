package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import testNG.ProjectMethodsTestNG;

public class ViewLead extends ProjectMethodsTestNG 
{
	public ViewLead()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.ID,using ="viewLead_firstName_sp")
	  WebElement eleExistingFirstname;
	public Login verifyUserName(String expectedFirstname)
	{
		//WebElement eleExistingUserName= locateElement("xpath", "//h2[text()='Demo Sales Manager']");
		verifyExactText(eleExistingFirstname, expectedFirstname);
		return new Login();
	}

}
