package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import testNG.ProjectMethodsTestNG;

public class MyLeads extends ProjectMethodsTestNG
{
	public MyLeads()
	{
		PageFactory.initElements(driver, this);
	}
    
	 @FindBy(linkText="Create Lead")
	  WebElement eleCreateLead;
	public CreateLead createLead()
	{
		click(eleCreateLead);
		return new CreateLead();
	}
}
