package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import testNG.ProjectMethodsTestNG;

public class MyHome extends ProjectMethodsTestNG
{
	public MyHome()
	{
		PageFactory.initElements(driver, this);
	}
	
	 @FindBy(linkText="Leads")
	  WebElement eleLeads;
	public MyLeads clickLeads()
	{
		//WebElement eleLeads = locateElement("linkText", "Leads");
		click(eleLeads);
		return new MyLeads();
	}

}
