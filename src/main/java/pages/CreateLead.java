package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import testNG.ProjectMethodsTestNG;

public class CreateLead extends ProjectMethodsTestNG 
{
   public CreateLead()
   {
	   PageFactory.initElements(driver, this);
   }
   
   @FindBy(how=How.ID,using ="createLeadForm_firstName")
   WebElement eleFirstname;
     public CreateLead typeFirstname(String data)
     {
  	    //WebElement eleCompany = locateElement("id", "createLeadForm_companyName");
  		type(eleFirstname, data);
  		return this;
     }
     
   
     @FindBy(how=How.ID,using ="createLeadForm_lastName")
     WebElement eleLastname;
       public CreateLead typeLastname(String data)
       {
    	    //WebElement eleCompany = locateElement("id", "createLeadForm_companyName");
    		type(eleLastname, data);
    		return this;
       }
       
   
       @FindBy(how=How.ID,using ="createLeadForm_companyName")
       WebElement eleCompany;
   public CreateLead typeCompanyname(String data)
   {
	    //WebElement eleCompany = locateElement("id", "createLeadForm_companyName");
		type(eleCompany, data);
		return this;
   }
   
   
   @FindBy(how=How.CLASS_NAME,using ="smallSubmit")
   WebElement eleSubmit;
   public ViewLead clickSubmit()
   {
 	  //WebElement eleLogin = locateElement("class", "decorativeSubmit");
 	  click(eleSubmit);
 	  return new ViewLead();
   }
	
	
}
