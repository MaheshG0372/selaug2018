package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import testNG.ProjectMethodsTestNG;

public class HomePage extends ProjectMethodsTestNG
{
	public HomePage()
	{
		PageFactory.initElements(driver, this);
	}
	
	  @FindBy(how=How.XPATH,using ="//h2[text()[contains(.,'Demo')]]")
	  WebElement eleExistingUserName;
	public HomePage verifyUserName(String expectedUsername)
	{
		//WebElement eleExistingUserName= locateElement("xpath", "//h2[text()='Demo Sales Manager']");
		verifyExactText(eleExistingUserName, expectedUsername);
		return this;
	}
	  @FindBy(linkText="CRM/SFA")
	  WebElement eleLink;
	public MyHome clickLinkCRMSFA()
	{
		//WebElement eleLink = locateElement("linkText", "CRM/SFA");
		click(eleLink);
		return new MyHome();
	}

}
