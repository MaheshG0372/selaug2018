package testcasesPOM;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.Login;
import testNG.ProjectMethodsTestNG;

public class TC001_CreateLead extends ProjectMethodsTestNG
{
	@BeforeTest
	public void setData()
	{
		testCaseName="TC001_CreateLead";
		testCaseDesc="Create a new Lead";
		category="smoke";
		author="mahesh";
		fileName="CreateLeadPOM";
	}
	
	@Test(dataProvider="fetchdata")
	public void Clead(String uname,String passwd,String vUsername, String fname,String lname,String cname,String vFirstname) 
	{
	  	new Login().typeUsername(uname).typePassword(passwd).clickLogin()
	  	.clickLinkCRMSFA().clickLeads().createLead().typeFirstname(fname).typeLastname(lname)
	  	.typeCompanyname(cname).clickSubmit().verifyUserName(vFirstname);
	}
	

}
