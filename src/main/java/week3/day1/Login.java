package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Login 
{

	public static void main(String[] args) 
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		try {
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.get("http://leaftaps.com/opentaps/");
			driver.findElementById("username").sendKeys("DemoSalesManager");
			driver.findElementById("password").sendKeys("crmsfa");
			driver.findElementByClassName("decorativeSubmit").click();
			driver.findElementByLinkText("CRM/SFA").click();
			driver.findElementByLinkText("Create Lead").click();
			driver.findElementById("createLeadForm_companyName").sendKeys("NMSWORKS");
			driver.findElementById("createLeadForm_firstName").sendKeys("Mahesh");
			driver.findElementById("createLeadForm_lastName").sendKeys("Gudla");
			WebElement src=  driver.findElementById("createLeadForm_dataSourceId");
			Select drop=new Select(src);
			drop.selectByVisibleText("Direct Mail");
			WebElement mc=driver.findElementById("createLeadForm_marketingCampaignId");
			Select mcobj=new Select(mc);
			List<WebElement> mcoptions=mcobj.getOptions();
			int size=mcoptions.size();
			mcobj.selectByIndex(size-2);
			driver.findElementByClassName("smallSubmit").click();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception thrown");
		}
		finally
		{
			driver.close();
		}
       
		
	}

}
