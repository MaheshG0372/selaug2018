package week5;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnHTMLReports {

	public static void main(String[] args) throws IOException 
	{
            ExtentHtmlReporter html=new ExtentHtmlReporter("./reports/testcase.html");
            html.setAppendExisting(true);
            ExtentReports ext=new ExtentReports();
            ext.attachReporter(html);
            
            ExtentTest test= ext.createTest("Testcase name","Test case Description");
            test.assignCategory("category");
            test.assignAuthor("author");
            test.pass("Browser Launched successfully" , MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
            test.fail("Failed test case", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img2.png").build());
            test.pass("Third test case" , MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img3.png").build());
            ext.flush();
	}

}
