package week0;

import java.util.Scanner;

public class LearningArithematicOperators
{

	public static void main(String[] args) 
	{
		System.out.print("Enter value of Number1  : ");
		Scanner scan=new Scanner(System.in);
		int number1=scan.nextInt();
		System.out.println("Number1 value " + number1);
		System.out.print("Enter value of Number2  : ");
		int number2=scan.nextInt();
		System.out.println("Number2 value " + number2);
		
		System.out.println("Addition value " + (number1+ number2));
		System.out.println("Addition value " + (number1 - number2));
		System.out.println("Multiplication value " + (number1 * number2));
		System.out.println("Division value " + (number1 / number2));
		System.out.println("Modulo value " + (number1 % number2));
	}

}
