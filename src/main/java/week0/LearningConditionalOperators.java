package week0;

import java.util.Scanner;

public class LearningConditionalOperators 
{

	public static void main(String[] args) 
	{
		System.out.print("Enter value of Number1  : ");
		Scanner scan=new Scanner(System.in);
		int number1=scan.nextInt();
		System.out.println("Number1 value " + number1);
		System.out.print("Enter value of Number2  : ");
		int number2=scan.nextInt();
		System.out.println("Number2 value " + number2);
		
		System.out.println("number1 greater than number2 " + (number1 > number2));
		System.out.println("number1 lesser than number2  " + (number1 < number2));
		System.out.println("number1 equals number2 " + (number1 == number2));
		System.out.println("number1  not equals number2 " + (number1 != number2));
		System.out.println("number1 greater than or equals number2 " + (number1 >= number2));
		System.out.println("number1 lesser than or equals number2  " + (number1 <= number2));
	}

}
