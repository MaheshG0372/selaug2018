package week0;

import java.util.Scanner;

public class EvenOrOdd
{

	public static void main(String[] args) 
	{
		System.out.print("Enter value of Number1  : ");
		Scanner scan=new Scanner(System.in);
		int number1=scan.nextInt();
		if(number1%2==0)
		{
			System.out.println(number1 + " is even number ");
		}
		else
		{
			System.out.println(number1 + " is odd number");
		}
	}

}
