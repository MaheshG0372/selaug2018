package week0;

public class LearningDataTypes {

	public static void main(String[] args) 
	{
		int x=120;
		// Range of int is -2147483648 to 2147483647 and 32bits
		System.out.println("Value of  integer x is " + x);
		byte b=26;
		//Range of byte is 128 to 127 and 8bits
		System.out.println("Value of byte b is " + b);
		boolean learning=true;
		//default value of boolean is true or false
		System.out.println("value of Boolean named learing is " + learning);
		double d=79.56;
		//Range of double is varies and 64bits
		System.out.println("Value of double d is " + d);
		float f=5.7f;
		//Range of float varies and 32bits
		System.out.println("Value of float f is " + f);
		long l=922631;
		//Range of long is huge and 64bits
		System.out.println("value of Long l is " + l);
		short s=2;
		//Range of short is -32768 to 32767
		System.out.println("Value of short s is " + s);
		char c='G';
		//Char is 16bits and range is 0 to 65535
		System.out.println("Value of char c is " + c);
		
		
	}

}
