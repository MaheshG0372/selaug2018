package testNG;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import wdMethods.ProjectMethods;
public class TC002_EditLead extends ProjectMethodsTestNG{
	@BeforeTest(groups= {"sanity"},dependsOnGroups= {"smoke"})
	public void setData1()
	{
		testCaseName="TC002_EditLead";
		testCaseDesc="Edit the Lead";
		category="sanity";
		author="mahesh";
		fileName="EditLead";
	}
	@Test(groups= {"sanity"},dataProvider="fetchdata")
	public void editLead(String name,String ddvalue) throws InterruptedException {
		click(locateElement("linkText","Leads"));
		click(locateElement("linkText","Find Leads"));
		type(locateElement("xpath", "(//input[@name='firstName'])[3]"),name);
		click(locateElement("xpath","//button[text()='Find Leads']"));
		Thread.sleep(7000);
		click(locateElement("xpath","//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']//a"));
		verifyTitle("View Leads");
		click(locateElement("linkText","Edit"));
		locateElement("id", "updateLeadForm_companyName").clear();
		type(locateElement("id", "updateLeadForm_companyName"),ddvalue);
		click(locateElement("xpath","//input[@type='submit']"));
		verifyExactText(locateElement("id", "viewLead_companyName_sp"), "NMS");
		closeBrowser();		
	}
}

