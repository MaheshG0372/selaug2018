package testNG;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import wdMethods.ProjectMethods;
import week6.day2.Readexcel;


public class TC001_CreateLeadTestNG extends ProjectMethodsTestNG{
	
	@BeforeTest(groups= {"smoke"})
	public void setData()
	{
		testCaseName="TC001_Createlead";
		testCaseDesc="Create a new Lead";
		category="smoke";
		author="mahesh";
		fileName="CLData";
	}
	
	
	@Test(groups= {"smoke"},dataProvider="fetchdata")
	public void Clead(String fname,String lname,String cname) {
		WebElement eleLead= locateElement("linkText", "Leads");
		click(eleLead);
		WebElement eleCreateLead= locateElement("linkText", "Create Lead");
		click(eleCreateLead);
		WebElement eleUsername = locateElement("id", "createLeadForm_firstName");
		type(eleUsername, fname);
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		type(eleLastName, lname);
		WebElement eleCompany = locateElement("id", "createLeadForm_companyName");
		type(eleCompany, cname);
		WebElement submit = locateElement("class", "smallSubmit");
		click(submit);
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		//click(eleLogout);
	}
	
	
	
}







