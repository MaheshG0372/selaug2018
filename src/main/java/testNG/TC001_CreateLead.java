package testNG;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import wdMethods.ProjectMethods;


public class TC001_CreateLead extends ProjectMethodsTestNG{
	
	@BeforeTest(groups= {"smoke"})
	public void setData()
	{
		testCaseName="TC001_Createlead";
		testCaseDesc="Create a new Lead";
		category="smoke";
		author="mahesh";
	}
	
	
	@Test(groups= {"smoke"})
	public void Clead() {
		WebElement eleLead= locateElement("linkText", "Leads");
		click(eleLead);
		WebElement eleCreateLead= locateElement("linkText", "Create Lead");
		click(eleCreateLead);
		WebElement eleUsername = locateElement("id", "createLeadForm_firstName");
		type(eleUsername, "Mahesh");
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		type(eleLastName, "G");
		WebElement elefirstNamelocal = locateElement("id", "createLeadForm_firstNameLocal");
		type(elefirstNamelocal, "Mahesh");
		WebElement eleLastNamelocal = locateElement("id", "createLeadForm_lastNameLocal");
		type(eleLastNamelocal, "G");
		WebElement eleSalutation = locateElement("id", "createLeadForm_personalTitle");
		type(eleSalutation, "Mahesh");
		WebElement source= locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(source, "Employee");
		WebElement eleTitle = locateElement("id", "createLeadForm_generalProfTitle");
		type(eleTitle, "Mr");
		WebElement eleRevenue = locateElement("id", "createLeadForm_annualRevenue");
		type(eleRevenue, "4000");
		WebElement industry= locateElement("id", "createLeadForm_industryEnumId");
		selectDropDownUsingText(industry, "Telecommunications");
		WebElement ownerShip= locateElement("id", "createLeadForm_ownershipEnumId");
		selectDropDownUsingIndex(ownerShip, 1);
		
		
		
		WebElement eleCompany = locateElement("id", "createLeadForm_companyName");
		type(eleCompany, "Nmsworks");
		WebElement submit = locateElement("class", "smallSubmit");
		click(submit);
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		//click(eleLogout);
	}
	
}







