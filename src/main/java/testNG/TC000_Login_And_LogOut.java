package testNG;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class TC000_Login_And_LogOut  extends ProjectMethodsTestNG
{
	@Test
	public void loginApp()
	{
	startApp("chrome", "http://leaftaps.com/opentaps");
	WebElement eleUserName = locateElement("id", "username");
	type(eleUserName, "DemoSalesManager");
	WebElement elePassword = locateElement("id","password");
	type(elePassword, "crmsfa");
	WebElement eleLogin = locateElement("class", "decorativeSubmit");
	click(eleLogin);
	}

}
