package testNG;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import week6.day2.Readexcel;

public class ProjectMethodsTestNG extends SeMethods{
	@BeforeSuite(groups= {"common"})
	public void beforeSuite() {
		beginResult();
	}
	@BeforeClass(groups= {"common"})
	public void beforeClass() {
		startTestCase();
	}
    
	@Parameters({"url"})
	@BeforeMethod(groups= {"common"})
	public void login(String url) {
		startApp("chrome",url);
		
	}
	@AfterMethod(groups= {"common"})
	public void closeApp() {
		closeBrowser();
	}
	@AfterSuite(groups= {"common"})
	public void afterSuite() {
		endResult();
	}
	@DataProvider(name="fetchdata")
	public Object[][] fetchData() throws IOException
	{
		return Readexcel.getExceldata(fileName);
	}
	
	
	
	
	
	
	
	
}
