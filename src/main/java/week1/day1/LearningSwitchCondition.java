package week1.day1;

import java.util.Scanner;

public class LearningSwitchCondition 
{

	public static void main(String[] args) 
	{
		Scanner scan= new Scanner(System.in);
	       System.out.print("Enter number1 :");
	       int n1=scan.nextInt();
	       System.out.print("Enter number2 :");
	       int n2=scan.nextInt();
	       System.out.print("Enter choice : ");
	       int choice=scan.nextInt();
	       switch(choice)
	       {
	       case 1:
	       {
	    	   System.out.println("addition value " + (n1+n2));
	    	   break;
	       }
	       case 2:
	       {
	    	   System.out.println("multiplication value " + (n1*n2));
	    	   break;
	       }
	       case 3:
	       {
	    	   System.out.println("substraction  value " + (n1-n2));
	    	   break;
	       
	       }
	       default : System.out.println("Invalid choice");
	}

	}
}
