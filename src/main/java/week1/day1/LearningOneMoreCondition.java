package week1.day1;

import java.util.Scanner;

public class LearningOneMoreCondition {

	public static void main(String[] args) 
	{
		Scanner scan= new Scanner(System.in);
	       System.out.print("Enter number1 :");
	       int n1=scan.nextInt();
	       System.out.print("Enter number2 :");
	       int n2=scan.nextInt();
	       System.out.print("Enter number3 :");
	       int n3=scan.nextInt();
	       if (n1>n2)
	       {
	    	   System.out.println(n1 + " is greatest");
	       }
	       else if (n2>n3)
	       {
	    	   System.out.println(n2 +" is greatest");
	       }
	       else if (n3>n1)
	       {
	    	   System.out.println(n3 + " is greatest");
	       }
	       
	}

}
