package week1.day1;

import java.util.Scanner;

public class SumofOddNumbers {

	public static void main(String[] args) 
	{
		Scanner scan= new Scanner(System.in);
	       System.out.print("Enter number1 :");
	       int n1=scan.nextInt();
	       int sum=0;
	       while(n1>0)
	       {
	    	   int remainder=n1%10;
	    	   if(remainder%2!=0)
	    	   {
	    		   sum=sum+remainder;
	    	   }
	    	   n1=n1/10;
	       }
	       System.out.println("sum of odd numbers is " + sum);
	}

}
