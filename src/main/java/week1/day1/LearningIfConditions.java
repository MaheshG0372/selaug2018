package week1.day1;

import java.util.Scanner;

public class LearningIfConditions {

	public static void main(String[] args) 
	{
       Scanner scan= new Scanner(System.in);
       System.out.print("Enter number1 :");
       int n1=scan.nextInt();
       System.out.print("Enter number2 :");
       int n2=scan.nextInt();
       if (n1>n2)
       {
    	   System.out.println(n1 + " is greater");
       }
       else
       {
    	   System.out.println(n2 + " is greater");
       }
	}

}
