package week1.day2;

public class LearningStringContains {

	public static void main(String[] args) 
	{
        String nameslist= "Mahesh,Nikhil,Raj,Maneesh,Suneel";
        String[] eachname=nameslist.split(",");
        for (String individualName : eachname)
        {
        	if(individualName.contains("a") || individualName.startsWith("S"))
        	{
        		System.out.println(individualName);
        	}
        
	}

}
}
